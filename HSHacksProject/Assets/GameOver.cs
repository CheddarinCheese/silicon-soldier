﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameOver : MonoBehaviour {

    public Text high;

	private void Awake()
	{
        high.text = "HIGH SCORE:" + GameMaster.highscore;
	}

	private void Update()
	{
        if(Input.GetMouseButton(0)){
            retry();
        }
	}
	public void retry(){
        SwipeDetector.reset();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Destroy(this.gameObject);
    }
}
