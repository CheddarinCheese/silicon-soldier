﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSet : MonoBehaviour {
    public GameObject tileSetContainer;
    public Sprite[] sprites;
    public SpriteRenderer[] nodes;
    public GameObject tileset;
    private void Awake()
	{
        foreach(SpriteRenderer renderer in nodes){
            renderer.sprite = sprites[Random.Range(0, sprites.Length - 1)];
        }

	}

	private void Update()
	{
        transform.Translate(new Vector3(-3f * Time.deltaTime, 0f, 0f));
        if(transform.position.x -19f<= -38f){
            GameObject free = Instantiate(tileset, tileSetContainer.transform) as GameObject;
            free.GetComponent<TileSet>().tileSetContainer = this.tileSetContainer;
            free.GetComponent<TileSet>().tileset = this.tileset;
            Destroy(this.gameObject);
        }
	}


}
