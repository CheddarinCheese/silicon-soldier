﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Player : MonoBehaviour {
    public AudioManager audioManager;
    public Animator animator;
    public GameMaster gameMaster;
    public GameObject playerDeath;
    public GameObject gameover;
    public bool invinsability = false;
	private void Awake()
	{
        SwipeDetector.OnSwipe += SwipeDetector_OnSwipe;
	}

	public void resetTrigger(string name)
    {
        animator.SetBool(name,false);

    }

    private void SwipeDetector_OnSwipe(SwipeData data)
    {
        animator.SetBool("Right", false);
        animator.SetBool("Left", false);
        animator.SetBool("Up", false);
        animator.SetBool("Down", false);

        StartCoroutine(setAnim(data));

        if (Physics2D.Raycast(new Vector2(-4f, 10f), Vector2.down))
        {
            Collider2D enemyCollider = Physics2D.Raycast(new Vector2(-4f, 10f),Vector2.down).collider;

            if (data.Direction == enemyCollider.GetComponentInParent<Enemy>().weakness)
            {
                enemyCollider.GetComponentInParent<Enemy>().kill();
                gameMaster.addScore(enemyCollider.GetComponent<Hit>().getPoint());
            }


        }
    }

    private IEnumerator setAnim(SwipeData data){
        yield return new WaitForEndOfFrame();
        if (data.Direction == SwipeDirection.Right)
        {
            animator.SetBool("Right", true);
            audioManager.play("Slash");
        }
        if (data.Direction == SwipeDirection.Left)
        {
            animator.SetBool("Left", true);
            audioManager.play("Dodge");
        }
        if (data.Direction == SwipeDirection.Up)
        {
            animator.SetBool("Up", true);
            audioManager.play("Slash");
        }
        if (data.Direction == SwipeDirection.Down)
        {
            animator.SetBool("Down", true);
            audioManager.play("Plug");
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.GetComponentInParent<Enemy>().kill();
        if (!invinsability)
        {
            int health = gameMaster.takeDamage();
            audioManager.play("TakeDamage");
            StartCoroutine(superMan());
            if (health <= 0)
            {
                audioManager.play("GameOver");
                if (GameMaster.highscore < gameMaster.score)
                    GameMaster.highscore = gameMaster.score;
                Instantiate(gameover);
                Instantiate(playerDeath,transform.position,Quaternion.identity);
                this.gameObject.SetActive(false);
            }
        }
	}

    private IEnumerator superMan(){
        invinsability = true;
        yield return new WaitForSeconds(.1f);
        invinsability = false;
    }

}
