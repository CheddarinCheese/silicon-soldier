﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour {

    /// <summary> The enemies prefabs. </summary>
    public GameObject[] enemiesPrefabs;

	public void spawn(int enemyType){
        if (enemyType == 0)
        {
            return;
        }
        
        GameObject enemy = Instantiate(enemiesPrefabs[enemyType - 1],this.transform) as GameObject;
    }

    public float enemyDelay(int index){
        if (index == 0)
            return 0;
        else
            return enemiesPrefabs[index - 1].GetComponent<Enemy>().time;
    }

}
