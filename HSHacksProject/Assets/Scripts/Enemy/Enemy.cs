﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public GameObject particleEffect;

    public SwipeDirection weakness;
    //How many units it travels per second
    //Travels 15units for perfect strike
    [SerializeField]
    private float speed;
    // The amount of time for it to reach crital point
    [HideInInspector]
    public float time;

	private void Awake()
	{
        time = 15f/speed;
	}

	private void Update()
	{
        transform.Translate(-Time.deltaTime * speed, 0, 0);

	}

    public void kill(){
        Debug.Log("Killed");
        GameObject particle = Instantiate(particleEffect,transform.position,Quaternion.identity) as GameObject;
        Destroy(particle, 5f);
        Destroy(this.gameObject);
    }


	public float getTime(){
        return 15 / speed;
    }
}
