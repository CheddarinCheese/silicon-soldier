﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour {
    private int health;
    public int score;
    public static int highscore;
    private int highScore;
    public bool isPlaying = true;
    public Text scoreText;
    public Sprite[] healthSprites;
    public Image healthUI;
	private void Awake()
	{
        health = 3;
        score = 0;
	}


    public int takeDamage(){
        
        health--;
        healthUI.sprite = healthSprites[health];
        return health;
    }

    public void addScore(int amount){
        score += amount;
        scoreText.text = "Score: " + score;
    }
}
