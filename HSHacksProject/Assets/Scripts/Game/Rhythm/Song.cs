﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="MySong",menuName = "Song")]
public class Song : ScriptableObject {

    /// <summary> Speed of the song and rhythm </summary>
    public int bpm;
    /// <summary> Start Beat </summary>
    public int startBeat = 8;
    /// <summary> Total Beats </summary>
    public int totalBeats;

    public List<string> pattern;
    public AudioClip musicClip;
}
