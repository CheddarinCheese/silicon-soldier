﻿using UnityEngine.Audio;
using System.Collections.Generic;
using UnityEngine;
using System;
public class AudioManager : MonoBehaviour {

    public Sound[] sounds;

	private void Awake()
	{
        foreach(Sound sound in sounds){
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;

            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = false;    
        }
        play("Walking");
	}

    public void play(string name){
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }

    
}

[System.Serializable]
public class Sound {
    public string name;
    public AudioClip clip;
    [Range(0f,1f)]
    public float volume;
    [Range(.1f,3f)]
    public float pitch;
    [HideInInspector]
    public AudioSource source;
}
