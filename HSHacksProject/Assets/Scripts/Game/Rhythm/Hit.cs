﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Hit : MonoBehaviour {

    public new string name;
    public int points;
    public GameObject particleEffect;

    public int getPoint(){
        GameObject a = Instantiate(particleEffect,transform.position,Quaternion.identity) as GameObject;
        a.GetComponentInChildren<Text>().text = name;
        Destroy(a, 4f);
        FindObjectOfType<AudioManager>().play(name);
        return points;
    }

}


