﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhythmManager : MonoBehaviour
{
    public WaveSpawner waveSpawner;
    ///<summary>Beats per second in our system</summary>
    public float bps;
    ///<summary> Current beat</summary>
    private int currentBeat;

    public Song song;
    [HideInInspector]
    public AudioSource source;

    private void Awake()
    {
        bps = song.bpm / 60f;
        currentBeat = 0;
        source = GetComponent<AudioSource>();
        source.clip = song.musicClip;
        source.Play();
    }
	
	private void Start()
	{
        StartCoroutine(spawnEnemies());
	}

	private IEnumerator spawnEnemies(){
        //Calculate what time to spawn at
        float time = (song.startBeat + currentBeat)/bps - waveSpawner.enemyDelay(getType(currentBeat));
        //Incrementing
        int index = currentBeat;
        currentBeat++;
        if(currentBeat<=song.totalBeats-song.startBeat){
            StartCoroutine(spawnEnemies());
        }
        yield return new WaitForSeconds(time);
        waveSpawner.spawn(getType(index));
    }

	private int getType(int index){
        return System.Int32.Parse(song.pattern[index / 8].Substring(index % 8, 1));
    }

	private static readonly RhythmManager instance = new RhythmManager();

    // Explicit static constructor to tell C# compiler
    // not to mark type as beforefieldinit
    static RhythmManager()
    {
    }

    private RhythmManager()
    {
    }

    public static RhythmManager Instance
    {
        get
        {
            return instance;
        }
    }

}
